import 'dart:convert';

import 'package:euroadapp/api/ApiProvider.dart';
import 'package:euroadapp/model/user.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  ApiProvider().urlBase = "http://localhost:3001/";

  final int userId = 26;
  group("LowLevel json user", () {
    String userJson;
    User user;
    test("Get one user json ", () async {
      var reponse = await ApiProvider()
          .ajaxGet(User().serviceName + "/" + userId.toString());
      userJson = reponse.body;

      expect(userJson, isNotEmpty);
    });

    test("User fromJson", () {
      user = User.fromJson(jsonDecode(userJson));
      expect(user.id, userId);
    });

    test("User to json", () {
      expect(user.toJson(), jsonDecode(userJson));
    });
  });

  group('User repo', () {
    test("Get one user  ", () async {
      final User user = await User().getOne(userId);

      expect(user.id, userId);
    });
  });
}
