import 'package:json_annotation/json_annotation.dart';

import 'ModelBase.dart';
part 'user.g.dart';

@JsonSerializable()
class User extends ModelBase {
  @override
  String get serviceName => "user";

  @override
  Map<String, dynamic> toJson() => _$UserToJson(this);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  @override
  fromJson(Map<String, dynamic> json) => User.fromJson(json);

  final int id;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String email;
  final String username;
  final String role;
  final String firstname;
  final String lastname;
  final DateTime birthday;
  final String photoUrl;
  final String phone;
  final String departement;
  final bool isActive;
  final String description;

  User({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.email,
    this.username,
    this.role,
    this.firstname,
    this.lastname,
    this.birthday,
    this.photoUrl,
    this.phone,
    this.departement,
    this.isActive,
    this.description,
  });
}
