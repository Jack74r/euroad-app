/* // To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

import 'package:euroadapp/model/ModelBase.dart';

class _User extends ModelBase {
  int id;
  DateTime createdAt;
  DateTime updatedAt;
  String email;
  String username;
  String role;
  String firstname;
  String lastname;
  DateTime birthday;
  String photoUrl;
  String phone;
  String departement;
  bool isActive;
  String description;

  _User({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.email,
    this.username,
    this.role,
    this.firstname,
    this.lastname,
    this.birthday,
    this.photoUrl,
    this.phone,
    this.departement,
    this.isActive,
    this.description,
  });

  @override
  String get serviceName => "user";

  _User userFromJson(String str) => _User.fromJson(json.decode(str));

  String userToJson(_User data) => json.encode(data.toJson());

  @override
  get fromJson => (str) => userFromJson(str);

  factory _User.fromJson(Map<String, dynamic> json) => _User(
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        email: json["email"],
        username: json["username"],
        role: json["role"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        birthday: DateTime.parse(json["birthday"]),
        photoUrl: json["photoUrl"],
        phone: json["phone"],
        departement: json["departement"],
        isActive: json["isActive"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "email": email,
        "username": username,
        "role": role,
        "firstname": firstname,
        "lastname": lastname,
        "birthday": birthday.toIso8601String(),
        "photoUrl": photoUrl,
        "phone": phone,
        "departement": departement,
        "isActive": isActive,
        "description": description,
      };
}

 */
