import 'dart:convert';

import 'package:euroadapp/api/ApiProvider.dart';
import 'package:http/http.dart' as http;

typedef ModelBaseDeserialization<T> = T Function(Map<String, dynamic>);

abstract class ModelBase<T> {
  String get serviceName;
  //ModelBaseDeserialization fromJson;
  // T Function fromJson;
  dynamic fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson();

  Future<T> getOne(int index) async {
    final http.Response response =
        await ApiProvider().ajaxGet('$serviceName/$index');
    T output;
    if (response.statusCode == 200) {
      output = fromJson(jsonDecode(response.body));
    } else {
      // Error Handling
    }
    return output;
  }
}
