part of 'navigation_bloc.dart';

@immutable
abstract class NavigationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadingEvent extends NavigationEvent {}

class HomeEvent extends NavigationEvent {}

class ProfilEvent extends NavigationEvent {}

class MapEvent extends NavigationEvent {}
