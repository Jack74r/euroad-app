part of 'navigation_bloc.dart';

@immutable
abstract class NavigationState {}

class NavigationnUninitialized extends NavigationState {}

class NavigationLoading extends NavigationState {
  String get title => "Loading";

  int get itemIndex => 0;
}

class NavigationHome extends NavigationState {
  String get title => "Home";
  int get itemIndex => 1;
}

class NavigationMap extends NavigationState {
  String get title => "Map";

  int get itemIndex => 2;
}

class NavigationProfil extends NavigationState {
  String get title => "Profil";

  int get itemIndex => 3;
}
