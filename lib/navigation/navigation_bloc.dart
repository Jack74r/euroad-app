import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

part 'navigation_event.dart';
part 'navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  @override
  NavigationState get initialState => NavigationHome();
  @override
  Stream<NavigationState> mapEventToState(
    NavigationEvent event,
  ) async* {
    switch (event.runtimeType) {
      case HomeEvent:
        yield NavigationHome();
        break;
      case LoadingEvent:
        yield NavigationLoading();
        break;
      case MapEvent:
        yield NavigationMap();
        break;
      case ProfilEvent:
        yield NavigationProfil();
        break;
      default:
    }
  }
}
