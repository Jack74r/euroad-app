import 'package:euroadapp/authentication/authentication.dart';
import 'package:euroadapp/page/home_page.dart';
import 'package:euroadapp/page/map_page.dart';
import 'package:euroadapp/page/profil_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'navigation_bloc.dart';

class NavigationPage extends StatelessWidget {
  const NavigationPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NavigationBloc navigationBloc =
        BlocProvider.of<NavigationBloc>(context);

    return Scaffold(
      body: BlocBuilder<NavigationBloc, NavigationState>(
        bloc: navigationBloc,
        builder: (BuildContext context, NavigationState state) {
          if (state is NavigationLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is NavigationHome) {
            return HomePage();
          }
          if (state is NavigationProfil) {
            return ProfilPage(
                avatarTag: false,
                user: (BlocProvider.of<AuthenticationBloc>(context)
                    .userRepository
                    .getCurrentUser()));
          }
          if (state is NavigationMap) {
            return MapPage();
          }
          if (state is NavigationnUninitialized) {
            return Text("NavigationnUninitialized");
          }
          return Container(
            child: Text("Error Navigation"),
          );
        },
      ),
      bottomNavigationBar: BlocBuilder<NavigationBloc, NavigationState>(
          bloc: navigationBloc,
          builder: (BuildContext context, NavigationState state) {
            return BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home, color: Colors.black),
                  title: Text('Home'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.map, color: Colors.black),
                  title: Text('Map'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.people, color: Colors.black),
                  title: Text('Profil'),
                ),
              ],
              onTap: (index) => {
                if (index == 0)
                  navigationBloc.add(HomeEvent())
                else if (index == 1)
                  navigationBloc.add(MapEvent())
                else if (index == 2)
                  navigationBloc.add(ProfilEvent())
              },
            );
          }),
    );
  }
}
