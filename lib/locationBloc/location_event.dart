part of 'location_bloc.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LocationInitializing extends LocationEvent {}

class LocationTurnedOn extends LocationEvent {}

class LocationTurnedOff extends LocationEvent {}

class LocationTurnedToggle extends LocationEvent {
  final LocationEvent currentState;

  LocationTurnedToggle(this.currentState);
}
