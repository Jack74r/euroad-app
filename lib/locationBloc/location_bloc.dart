import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';
import 'package:euroadapp/api/ApiProvider.dart';
import 'package:euroadapp/authentication/auth_user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import './locationUtil/dialog.dart' as util;
part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  @override
  LocationState get initialState => LocationDisabled();

  @override
  Stream<LocationState> mapEventToState(
    LocationEvent event,
  ) async* {
    if (event is LocationInitializing) {
      await initBackgroundGeolocation();
      yield LocationDisabled();
    } else if (event is LocationTurnedToggle) {
      if (event.currentState is LocationTurnedOn) {
        yield LocationDisabled();
      } else {
        yield LocationEnabled();
      }
    } else if (event is LocationTurnedOn) {
      try {
        await bg.BackgroundGeolocation.start();
        await bg.BackgroundGeolocation.changePace(true);
        yield LocationEnabled();
      } catch (e) {
        yield LocationDisabled();
        BotToast.showText(
            duration: Duration(seconds: 5),
            text:
                "L'application à besoin de l'autorisation GPS pour fonctionner.");
      }
    } else if (event is LocationTurnedOff) {
      await bg.BackgroundGeolocation.changePace(false);
      var v = await bg.BackgroundGeolocation.stop();
      yield LocationDisabled();
    }
  }

  void initBackgroundGeolocation() async {
    // Fired whenever a location is recorded
    bg.BackgroundGeolocation.onLocation((bg.Location location) {
      BotToast.showText(text: "📍" + location.toString());
      print('[location] - $location');
    });

    // Fired whenever the plugin changes motion-state (stationary->moving and vice-versa)
    bg.BackgroundGeolocation.onMotionChange((bg.Location location) {
      BotToast.showSimpleNotification(title: "🚀" + "OnMotionChange");

      print('[motionchange] - $location');
    });

    // Fired whenever the state of location-services changes.  Always fired at boot
    bg.BackgroundGeolocation.onProviderChange((bg.ProviderChangeEvent event) {
      BotToast.showSimpleNotification(title: "" + "onProviderChange");
      print('[providerchange] - $event');
    });

    bg.BackgroundGeolocation.onHttp(_onHttp);
    bg.BackgroundGeolocation.onConnectivityChange(_onConnectivityChange);

    bg.State bgState = await bg.BackgroundGeolocation.ready(bg.Config(
        // Logging & Debug
        reset: false,
        debug: true,
        logLevel: bg.Config.LOG_LEVEL_VERBOSE,
        // Geolocation options
        desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
        distanceFilter: 10.0,
        stopTimeout: 1,
        // HTTP & Persistence
        url: "${ApiProvider().urlBase}position",
        //url: "http://tracker.transistorsoft.com/api/locations",
        authorization: bg.Authorization(
          strategy: "JWT",
          accessToken: await UserRepository().getToken(),
        ),
        httpRootProperty: ".",
        locationTemplate: """{
          "latitude": "<%= latitude %>",
          "longitude": "<%= longitude %>",
          "accuracy": "<%= accuracy %>",
          "speed": "<%= speed %>",
          "is_moving": "<%= is_moving %>",
          "altitude": "<%= altitude %>",
          "altitude_accuracy": "<%= altitude_accuracy %>",
          "timestamp": "<%= timestamp %>",
          "mock": "<%= mock %>",
          "event": "<%= event %>",
          "odometer": "<%= odometer %>",
          "activity_type": "<%= activity.type %>",
          "activity_confidence": "<%= activity.confidence %>",
          "battery_level": "<%= battery.level %>"    ,     
          "battery_is_charging": "<%= battery.is_charging %>"}          
          """,
        extras: {"team": "3", "user": "1"},
        autoSync: true,
        // Application options
        stopOnTerminate: false,
        startOnBoot: true,
        enableHeadless: true,
        heartbeatInterval: 60));
  }

  void _onHttp(bg.HttpEvent event) async {
    int status = event.status;
    bool success = event.success;
    String responseText = event.responseText;
    BotToast.showSimpleNotification(title: (success ? "✅" : "❌") + " HTTP");
    print(
        '[onHttp] status: ${status}, success? ${success}, responseText: ${responseText}');
  }

  void _onConnectivityChange(bg.ConnectivityChangeEvent event) {
    BotToast.showSimpleNotification(title: "📶" + "_onConnectivityChange");
    print('[${bg.Event.CONNECTIVITYCHANGE}] - $event');
  }
}
