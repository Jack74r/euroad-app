part of 'location_bloc.dart';

abstract class LocationState extends Equatable {
  @override
  List<Object> get props => null;
  const LocationState();
}

class LocationInitial extends LocationState {}

class LocationInitialized extends LocationState {}

class LocationEnabled extends LocationState {
  LocationEnabled() {
    bg.BackgroundGeolocation.playSound(util.Dialog.getSoundId("BUTTON_CLICK"));
    BotToast.showText(text: "✅" + "LocationState LocationEnabled");
  }
}

class LocationDisabled extends LocationState {
  LocationDisabled() {
    bg.BackgroundGeolocation.playSound(util.Dialog.getSoundId("BUTTON_CLICK"));
    BotToast.showText(text: "❌" + "LocationState LocationDisabled");
  }
}
