import 'package:euroadapp/model/user.dart';
import 'package:euroadapp/widgets/profil.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ProfilPage extends StatelessWidget {
  const ProfilPage({Key key, this.user, this.avatarTag}) : super(key: key);
  final Object avatarTag;
  final User user;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Profil")),
      body: Container(
        child: Profil(
          user: user,
          avatarTag: avatarTag,
        ),
      ),
    );
  }
}
