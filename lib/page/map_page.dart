import 'package:euroadapp/locationBloc/location_bloc.dart';
import 'package:euroadapp/navigation/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;
import 'package:euroadapp/locationBloc/locationUtil/dialog.dart' as util;

class MapPage extends StatelessWidget {
  const MapPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LocationBloc locationBloc = BlocProvider.of<LocationBloc>(context);

    return BlocBuilder<LocationBloc, LocationState>(
      builder: (context, state) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Map"),
              actions: <Widget>[_buildSwitch(state, locationBloc)],
            ),
            body: Container(
              child: IconButton(
                icon: Icon(Icons.gps_fixed),
                onPressed: _onClickGetCurrentPosition,
              ),
            ));
      },
    );
  }

  void _onClickGetCurrentPosition() async {
    bg.BackgroundGeolocation.playSound(util.Dialog.getSoundId("BUTTON_CLICK"));

    bg.BackgroundGeolocation.getCurrentPosition(
        persist: true, // <-- do not persist this location
        desiredAccuracy: 40, // <-- desire an accuracy of 40 meters or less
        maximumAge: 10000, // <-- Up to 10s old is fine.
        timeout: 30, // <-- wait 30s before giving up.
        samples: 3, // <-- sample just 1 location
        extras: {"event": "manual"}).then((bg.Location location) {
      print('[getCurrentPosition] - $location');
    }).catchError((error) {
      print('[getCurrentPosition] ERROR: $error');
    });
  }

  Widget _buildSwitch(LocationState state, LocationBloc locationBloc) {
    if (state is LocationDisabled) {
      return Switch(
          value: false,
          onChanged: (value) {
            locationBloc.add(LocationTurnedOn());
          });
    } else if (state is LocationEnabled) {
      return Switch(
          value: true,
          onChanged: (value) {
            locationBloc.add(LocationTurnedOff());
          });
    } else {
      return Container();
    }
  }
}
