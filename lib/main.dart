import 'package:euroadapp/navigation/navigation_page.dart';
import 'package:flutter/material.dart';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
//    as bg;

import 'authentication/auth_user_repository.dart';
import 'authentication/authentication.dart';
import 'locationBloc/location_bloc.dart';
import 'login/login_page.dart';
import 'navigation/navigation_bloc.dart';
import 'page/home_page.dart';
import 'page/splash_page.dart';
import 'package:bot_toast/bot_toast.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final userRepository = UserRepository();
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) {
        return AuthenticationBloc(userRepository: userRepository)
          ..add(AppStarted());
      },
      child: App(userRepository: userRepository),
    ),
  );
}

class App extends StatelessWidget {
  final UserRepository userRepository;

  App({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.red,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      builder: BotToastInit(),
      navigatorObservers: [BotToastNavigatorObserver()],
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            return MultiBlocProvider(
              providers: [
                BlocProvider<NavigationBloc>(
                  create: (context) => NavigationBloc()..add(HomeEvent()),
                ),
                BlocProvider<LocationBloc>(
                  create: (context) =>
                      LocationBloc()..add(LocationInitializing()),
                ),
              ],
              child: NavigationPage(),
            );
          }
          if (state is AuthenticationUnauthenticated) {
            return LoginPage(userRepository: userRepository);
          }
          if (state is AuthenticationLoading) {
            return Center(child: CircularProgressIndicator());
          }
          return SplashPage();
        },
      ),
    );
  }
}
