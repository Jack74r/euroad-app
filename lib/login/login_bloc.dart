import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:euroadapp/authentication/auth_user_repository.dart';
import 'package:euroadapp/model/user.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:euroadapp/authentication/authentication.dart';

import 'login.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final String token = await userRepository.authenticate(
          email: event.email,
          password: event.password,
        );
        final User currentUser = await userRepository.getUserFromToken(token);
        authenticationBloc
            .add(LoggedIn(token: token, currentUser: currentUser));
        yield LoginInitial();
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}
