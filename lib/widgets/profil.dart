import 'package:cached_network_image/cached_network_image.dart';
import 'package:euroadapp/model/user.dart';
import 'package:euroadapp/widgets/cardCustom.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//https://slcoderlk.blogspot.com/2019/01/beautiful-user-profile-material-ui-with.html
class Profil extends StatelessWidget {
  const Profil({Key key, this.user, this.avatarTag}) : super(key: key);

  final User user;
  final Object avatarTag;

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          _buildAvatar(),
          SizedBox(height: 10.0),
          _buildUserName(),
          _buildFullName(),
          _buildStatContainer(),
          SizedBox(height: 10.0),
          _buildBio(),
          _buildPlayedEvent(),
        ],
      ),
    ]);
  }

  Widget _buildPlayedEvent() {
    return CardCustom(
      urlImage:
          "https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/5b95be27be7744fb5c8b467b.jpg",
      title: "Titre ..",
      description: "Description ...",
    );
  }

  Widget _buildAvatar() {
    return CachedNetworkImage(
      imageUrl: user.photoUrl ?? "",
      placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
        color: Color(0xFFEFF4F7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("Participation", "0"),
          _buildStatItem("Km", "0"),
          _buildStatItem("Defis", "0"),
        ],
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildBio() {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400, //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      padding: EdgeInsets.all(8.0),
      child: Text(
        user.description ??
            "Le Lorem Imorceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.",
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _buildUserName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );
    return Text(
      "${user.username}",
      style: _nameTextStyle,
    );
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 12.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      "${user.firstname} ${user.lastname}",
      style: _nameTextStyle,
    );
  }
}
