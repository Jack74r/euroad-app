import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardCustom extends StatelessWidget {
  const CardCustom(
      {Key key,
      this.urlImage,
      this.title,
      this.description,
      this.bp1,
      this.bp2})
      : super(key: key);
  final String urlImage;
  final String title;
  final String description;
  final FlatButton bp1;
  final FlatButton bp2;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 2,
          clipBehavior: Clip.antiAlias,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 184,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: CachedNetworkImage(
                        imageUrl: urlImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      bottom: 16,
                      left: 16,
                      right: 16,
                      child: FittedBox(
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          title,
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // Description and share/explore buttons.
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // This array contains the three line description on each card
                    // demo.
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        description,
                      ),
                    ),
                  ],
                ),
              ),

              // share, explore buttons
              ButtonBar(
                alignment: MainAxisAlignment.start,
                children: [bp1, bp2],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
