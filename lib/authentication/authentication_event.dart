import 'package:euroadapp/model/user.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  final String token;
  final User currentUser;
  const LoggedIn({@required this.token, @required this.currentUser});

  @override
  List<Object> get props => [token, currentUser];

  @override
  String toString() => 'LoggedIn { user : ${currentUser.email}| token: $token}';
}

class LoggedOut extends AuthenticationEvent {}
