import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:euroadapp/model/user.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'authentication.dart';
import 'auth_user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;
  AuthenticationBloc({@required this.userRepository})
      : assert(userRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    //BotToast.showText(text: event.toString(), duration: Duration(seconds: 3));
    if (event is AppStarted) {
      final bool hasToken = await userRepository.hasToken();

      if (hasToken) {
        try {
          final String token = await userRepository.getToken();
          userRepository.currentUser = await userRepository.getUser();
          yield AuthenticationAuthenticated();
        } catch (e) {
          yield AuthenticationUnauthenticated();
        }
      } else {
        yield AuthenticationUnauthenticated();
      }
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await userRepository.persistToken(event.token);
      userRepository.currentUser = await userRepository.getUser();

      yield AuthenticationAuthenticated();
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await userRepository.deleteToken();
      yield AuthenticationUnauthenticated();
    }
  }
}
