import 'dart:async';
import 'dart:convert';
import 'package:euroadapp/api/ApiProvider.dart';
import 'package:euroadapp/api/errorResponse.dart';
import 'package:euroadapp/model/user.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:euroadapp/utils/constants.dart' as constants;
import 'package:http/http.dart';
import 'package:meta/meta.dart';

class UserRepository {
  User currentUser;

  Future<String> authenticate({
    @required String email,
    @required String password,
  }) async {
    var loginObject = {
      'email': email,
      'password': password,
    };
    var response =
        await ApiProvider().ajaxPost('auth/login', jsonEncode(loginObject));

    if (response.statusCode == 201) {
      final String token = jsonDecode(response.body)['token'];

      await setUser(await getUserFromToken(token));

      /*  final User user = await checkLoggedUser(token);
      if (user != null) {
        await setUser(user);
      } else {
        await setUser(User());
      } */
    } else {
      throw ErrorResponse.fromJson(jsonDecode(response.body));
    }
    return jsonDecode(response.body)['token'];
  }

  Future<void> setUser(User user) async {
    final storage = FlutterSecureStorage();

    await storage.write(key: constants.storageKeyUser, value: jsonEncode(user));
  }

  Future<User> getUser() async {
    final storage = FlutterSecureStorage();

    String userJson = await storage.read(key: constants.storageKeyUser);

    return User.fromJson(jsonDecode(userJson));
  }

  Future<User> getUserFromToken(String token) async {
    Response response =
        await http.post(ApiProvider().urlBase + 'auth/me', body: '', headers: {
      // 'X-DEVICE-ID': await _getDeviceIdentity(),
      //  'X-TOKEN': await _getMobileToken(),
      //  'X-APP-ID': _applicationId,
      'Authorization': 'Bearer ${token}',
      'Content-Type': 'application/json; charset=utf-8'
    });
    if (response.statusCode != 201) {
      throw ErrorResponse.fromJson(jsonDecode(response.body));
    }
    return User().fromJson(jsonDecode(response.body));
  }

  Future<void> deleteToken() async {
    final storage = FlutterSecureStorage();

    /// delete from keystore/keychain
    await storage.delete(key: constants.storageKeyMobileToken);
    return;
  }

  Future<void> persistToken(String token) async {
    final storage = FlutterSecureStorage();

    /// write to keystore/keychain
    await storage.write(key: constants.storageKeyMobileToken, value: token);
    return;
  }

  Future<String> getToken() async {
    /// read from keystore/keychain
    final storage = FlutterSecureStorage();
    final token = await storage.read(key: constants.storageKeyMobileToken);

    return token;
  }

  Future<bool> hasToken() async {
    /// read from keystore/keychain
    final storage = FlutterSecureStorage();
    final token = await storage.read(key: constants.storageKeyMobileToken);
    if (token != null) {
      return token.isNotEmpty;
    }
    return false;
  }

  getCurrentUser() {
    return currentUser;
  }
}
