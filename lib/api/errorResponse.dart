import 'package:json_annotation/json_annotation.dart';
part 'errorResponse.g.dart';

@JsonSerializable()
class ErrorResponse {
  final int statusCode;
  final String error;
  final String message;

  ErrorResponse({
    this.statusCode,
    this.error,
    this.message,
  });
  @override
  String toString() => 'ErrorResponse : $error $statusCode\n$message';
  Map<String, dynamic> toJson() => _$ErrorResponseToJson(this);

  factory ErrorResponse.fromJson(Map<String, dynamic> json) =>
      _$ErrorResponseFromJson(json);
}
