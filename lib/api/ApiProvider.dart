import 'dart:convert';
import 'dart:developer';

//import 'package:euroadapp/model/User.dart';
import 'package:euroadapp/authentication/auth_user_repository.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ApiProvider {
  // the URL of the Web Server
  String urlBase = "http://10.0.2.2:3001/";
  //String urlBase = "http://192.168.1.53:3001/";
  ApiProvider._internal();
  static final ApiProvider _apiProvider = ApiProvider._internal();
  factory ApiProvider() => _apiProvider;

  // the storage key for the token

  /* Future<User> me() async {
    User user;
    try {
      http.Response response = await http.post(urlBase + 'me');
      if (response.statusCode == 201) {
        user = User.fromJson(jsonDecode(response.body));
      } else {
        //TODO handle error
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  Future<bool> setUser(User user) async {
    final SharedPreferences prefs = await _prefs;

    return prefs.setString(_storageKeyUser, jsonEncode(user));
  }

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<User> getUser() async {
    final SharedPreferences prefs = await _prefs;
    final String sUser = prefs.getString(_storageKeyUser) ?? '';
    User user;
    if (sUser != '') {
      user = User.fromJson(jsonDecode(sUser));
    }
    return user;
  }

  Future<bool> setMobileToken(String token) async {
    final SharedPreferences prefs = await _prefs;

    return prefs.setString(_storageKeyMobileToken, token);
  }

  Future<String> getMobileToken() async {
    final SharedPreferences prefs = await _prefs;

    return prefs.getString(_storageKeyMobileToken) ?? '';
  }

  Future<bool> login(String email, String password) async {
    bool isLogged = false;
    var loginObject = {
      'email': email,
      'password': password,
    };
    log("$loginObject");
    var response =
        await http.post(urlBase + 'auth/login', body: jsonEncode(loginObject));

    if (response.statusCode == 201) {
      await setMobileToken(jsonDecode(response.body)['token']);
      final User user = await me();
      if (user != null) {
        await setUser(user);
        isLogged = true;
      } else {
        await setMobileToken("");
        await setUser(User());
      }
    } else {
      isLogged = false;
    }
    return isLogged;
  } */

  /// ----------------------------------------------------------
  /// Http "GET" request
  /// ----------------------------------------------------------
  Future<http.Response> ajaxGet(String url) async {
    return await http.get(urlBase + '$url', headers: {
      //      'X-DEVICE-ID': await _getDeviceIdentity(),
      //  'X-TOKEN': await _getMobileToken(),
      //  'X-APP-ID': _applicationId
      'Authorization': 'Bearer ${UserRepository().getToken()}',
    });
  }

  /// ----------------------------------------------------------
  /// Http "POST" request
  /// ----------------------------------------------------------
  Future<http.Response> ajaxPost(String url, String data) async {
    http.Response response =
        await http.post(urlBase + '$url', body: data, headers: {
      // 'X-DEVICE-ID': await _getDeviceIdentity(),
      //  'X-TOKEN': await _getMobileToken(),
      //  'X-APP-ID': _applicationId,
      'Authorization': 'Bearer ${UserRepository().getToken()}',
      'Content-Type': 'application/json; charset=utf-8'
    });
    return response;
  }

  Future<bool> handShake() async {
    bool tokenIsOk = false;
    http.Response response = await ajaxPost(urlBase + 'me', '');
    if (response.statusCode == 201) {
      tokenIsOk = true;
    } else {
      tokenIsOk = false;
    }

    return tokenIsOk;
  }
}
